# HTML  --超文本语言







# CSS  --层叠样式表







# JavaScript --解释性脚本语言







# Vue  --前端框架

## 组件传值

### 1. provied/inject  父组件给子组件注入属性

 1. 父组件

    ~~~javascript
     provide() {
        return {
          father: this
        };
      }
    ~~~

    

 2. 子组件

    ~~~javascript
    inject:['father'],
    ~~~


### 事件

|  事件名称  |   事件作用   |
| :--------: | :----------: |
| mousedown  |   鼠标按下   |
|  mouseup   |   鼠标抬起   |
|   click    |   鼠标点击   |
|  dblclick  |   鼠标双击   |
| mousemove  |   鼠标移动   |
| mouseleave |   鼠标离开   |
|  mouseout  |   鼠标移出   |
| mouseenter |  鼠标在内部  |
| mouseover  | 鼠标离开内部 |

## watch属性监听

```javascript
 watch: {
     // 被监听属性名称
    cityName: {
        // 新值    旧值
      handler(newName, oldName) {
      // ...
    },
    deep: true,  //深度监听
    immediate: true  //开局监听
    }
  } 
```

## slot --插槽









# Node.js  --后端服务器









# Npm --包管理工具

## 自定义vue组件库

~~~javascript
// 根目录创建 packages 目录 该目录为组件的主要文件 所有组件都存放在这里
// 根目录新建一个  vue.config.js  配置文件
~~~

1.   根目录创建 packages 目录 该目录为组件的主要文件 所有组件都存放在这里

   1.1 将所有的组件和字体图标放入 packages 中   （文档：vue的install方法 官网查找）

   1.2 在 packages 中新建一个 index.js 

   ~~~ javascript
   // 整个自定义组件库的一个入口
   
   // 导入所有组件
   import Steep from './Steep'
   // 对应的字体文件也得导入
   // 例如   import '**/***.css'
   
   // 将所有的组件放入数组中 利于遍历注册
   const components = [
       Steep,
   ]
   
   // 必须导出一个 install 方法 或者对象中又 install 方法
   let install = function(Vue){
       // 原理就是 在 vue框架中 调用 Vue.use()  时 会执行这个函数  我们在这个函数中
       // 完成对应的配置即可  注册所有的组件
       components.forEach(item => {
           Vue.component(item.name,item)
       })
       console.log(123)
   
   }
   // 这一步是为了 如果直接引入文件 就不用调用 Vue.use()
   if(typeof window != 'undefined' && window.Vue){
       install(window.Vue)
   }
   // 导出
   export default  {
       install
   }
   ~~~

   

2. 根目录新建一个  vue.config.js  配置文件

   ~~~javascript
   const path = require('path')
   module.exports = {
       // 扩展 webpack 配置 ，使 packages 加入编译
       chainWebpack:config => {
           config.module
           .rule('js')
           .include.add(path.resolve(__dirname,'packages')).end()
           .use('babel')
           .loader('babel-loader')
           .tap(options => {
               // 修改
               return options
           })
       }
   }
   ~~~

   2. 1对 packages 进行打包 想要构建库 需要在 package.json 中加入一行命令 指定打包文件

       ~~~json
   "lib":"vue-cli-service build --target lib packages/index.js"
    // 并将name改为想要的名字  必须是npm上没有的
   "name":"zs-ui"
       ~~~

   ~~~npm
   执行打包命令   npm run lib
   ~~~

   3. 将代码放到GitHub上去

   4. 发布到 Npm 上

   ​	4.1  首先修改package.json 上的  "private": false,

   ​	4.2  指定包的入口文件在 package.json中添加一行      "main": "dist/zs-ui.umd.js",

   ​	4.3 在根目录下创建 .npmignore 文件   忽略某些文件不被上传到 npm

   ~~~text
   # 忽略目录
   src/
   public/
   packages/
   
   # 忽略指定文件
   *.map
   vue.config.js
   babel.config.js
   ~~~

   ​	4.4 上传到npm

   ~~~npm
   nrm sl   // 查看npm源
   npm login  // 登录npm
   npm publish   // 上传  每次上传的版本号必须不一样
   ~~~

   ![image-20200319075124424](mdimgs/image-20200319075124424.png)

   4.5 如果报错结局流程

    检查是否是淘宝镜像 npm config get registry

    如果是 http://registry.npm.taobao.org/  那么就需要修改

   `npm config set registry=http://registry.npmjs.org`

   并重新登录

   还可能要验证电子邮箱  登录你注册npm是用的邮箱 收件箱里进行验证

   ![image-20200319081723463](mdimgs/image-20200319081723463.png)

   完成~！

   提交并切换会淘宝镜像 ``npm config set registry https://registry.npm.taobao.org`

   
   
   

