## String--字符串

### 获取字符长度

~~~ javascript
var txt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var sln = txt.length; // 26
~~~

### 获取字符串索引位置

~~~javascript
/*==========从左往右查找第一次出现的位置 indexOf()============*/
var str = "The full name of China is the People's Republic of China.";
var pos = str.indexOf("China");  // 17
/*==========从右往左查找第最后一次出现的位置 lastIndexOf()============*/
var str = "The full name of China is the People's Republic of China.";
var pos = str.lastIndexOf("China");  // 51

// 未找到均返回 -1
//两种方法都接受作为检索起始位置的第二个参数。
var str = "The full name of China is the People's Republic of China.";
var pos = str.indexOf("China", 18);
var str = "The full name of China is the People's Republic of China.";
var pos = str.lastIndexOf("China", 50);
~~~

### 提取部分字符串

* slice --提取字符串的某个部分并在新字符串中返回被提取的部分。

  该方法设置两个参数：起始索引（开始位置），终止索引（结束位置）。

  ~~~ javascript
  var str = "Apple, Banana, Mango";
  var res = str.slice(1,5);   //pple    截取不包括结束索引的字符
  /*如果某个参数为负，则从字符串的结尾开始计数。*/
  var str = "Apple, Banana, Mango";
  var res = str.slice(-5,-1); // Mang  一样从左往右提取
  /*如果省略第二个参数，则该方法将裁剪字符串的剩余部分：*/
  var str = "Apple, Banana, Mango";
  var res = str.slice(1); // pple, Banana, Mango
  ~~~

* substr() 类似于 slice() 不同之处在于第二个参数规定被提取部分的*长度*。

  ~~~javascript
  var str = "Apple, Banana, Mango";
  var res = str.substr(0,5); // Apple
  /*如果省略第二个参数，则该 substr() 将裁剪字符串的剩余部分。*/
  ~~~

* match  正则表达式截取字符串

  ~~~javascript
  var str="1 plus 2 equal 3"
  console.log(str.match(/\d+/g))
  // 输出  1，2，3
  // 截取文件后缀
  var str="sadfasdf.gif"
  console.log(str.match(/\.\w*$/g)[0])
  ~~~

  

### 替换字符串-replace()

~~~javascript
str = "Please visit Microsoft!";
var n = str.replace("Microsoft", "W3School");
/*如需替换所有匹配，请使用正则表达式的 /g 标志（用于全局搜索）：*/
/*如需执行大小写不敏感的替换，请使用正则表达式 /i（大小写不敏感）：*/
~~~

### 转换大小写

* 通过 toUpperCase() 把字符串转换为大写：

  ~~~javascript
  var text1 = "Hello World!";       // 字符串
  var text2 = text1.toUpperCase();  // text2 是被转换为大写的 text1
  ~~~

* 通过 toLowerCase() 把字符串转换为小写：

  ~~~javascript
  var text1 = "Hello World!";       // 字符串
  var text2 = text1.toLowerCase();  // text2 是被转换为小写的 text1
  ~~~

### 删除字符串两端的空白符 -trim()

~~~ javascript
var str = "       Hello World!        ";
console.log(str.trim());
~~~

### 符串中指定索引的字符 unicode 编码

~~~javascript
var str = "HELLO WORLD";

str.charCodeAt(0);         // 返回 72
~~~

### 字符串转换为数组

~~~javascript
var txt = "a,b,c,d,e";   // 字符串
txt.split(",");          // 用逗号分隔
txt.split(" ");          // 用空格分隔
txt.split("|");          // 用竖线分隔
~~~





##  Number--数值

## Object--对象

### 克隆对象

~~~javascript
function clone(Obj) {
  var buf;   
  if (Obj instanceof Array) {       
    buf = [];  
    // 创建一个空的数组
    var i = Obj.length;  
    while (i--) { 
      buf[i] = clone(Obj[i]);     
    }
    return buf;
  } else if (Obj instanceof Object){
    buf = {};  
    // 创建一个空对象      
    for(var k in Obj) {  
      // 为这个对象添加新的属性          
      buf[k] = clone(Obj[k]);       
    }    
    return buf;
  } else {
    return Obj;  
  }
}
~~~



## Array--数组
### map --用来对每一个值做动作

  ~~~javascript
  let arr = [1,2,3,4,5,6,7,8,9]
  let arr1 =  arr.map((item) => {  // 给arr数组中的每一个值加上1
      return item+1
  })
  ~~~

### filter --用来过滤数组

  ~~~javascript
  let arr = [1,2,3,4,5,6,7,8,9]
  let arr1 =  arr.filter((item) => {  // 当条件表达式为true就保留 否则 就过滤掉
      return item%2 == 0
  })
  ~~~

### findIndex --找到对应元素在数组中的索引

  ~~~javascript
  let arr = [1,2,3,4,5,6,7,8,9]
  let index =  arr.findIndex((item) => {  // 当条件表达式为true的时候返回索引
      return item == 2
  })
  ~~~

### every --判断所有元素

  ~~~javascript
  let arr = [1,2,3,4,5,6,7,8,9]
  let flage =  arr.every((item) => {  //当所有元素 与当前表达式 都成立的情况返回true
      return isNaN(item)
  })
  ~~~

### reverse --反转数组

 ~~~javascript
 var arr2 = [13, 24, 51, 3]; ``console.log(arr.reverse()); ``//[3, 51, 24, 13]``
 ~~~

### 连接数组

```javascript
 var arr = [1,3,5,7];
 var arrCopy = arr.concat(9,[11,13]);
 console.log(arrCopy); ``//[1, 3, 5, 7, 9, 11, 13]
 console.log(arr); ``// [1, 3, 5, 7](原数组未被修改)
```

### splice --删除替换插入

~~~javascript
var arr = [1,3,5,7,9,11];
// 删除
var arrRemoved = arr.splice(0,2);
console.log(arr); //[5, 7, 9, 11]
console.log(arrRemoved); //[1, 3]
// 插入
var arrRemoved2 = arr.splice(2,0,4,6);
console.log(arr); // [5, 7, 4, 6, 9, 11]
console.log(arrRemoved2); // []
~~~

### 头部和尾部插入修改元素

~~~javascript
let arr = [1,2,3,4,11,22,18,19,112,110]   
arr.unshift(8)   // 给数组头添加一个元素
arr.push(9)    // 给数组的尾部添加一个元素
arr.pop()       // 删除数组尾部的元素 并返回删除的元素
arr.shift()   //  删除第一元素  并返回删除的元素
~~~

### sort --排序

~~~javascript
console.log(goods.sort((a,b)=>{   // a-b 升序      b-a 降序
    return a.price - b.price
}))
~~~







## Boolean--布尔值

## 常用内置方法

### 判断数据类型的表达式

> Obj instanceof Object     // 判断Obj 是不是  对象  返回   True  或者  False

### 获取数据类型

> typeof('1')    // 返回对应的数据类型   对象和数值 都返回 Object

### while循环

> let i = 5;while(i--){console.log(i)};   // i-- 为零的时候表示false

### jquery --在线地址

~~~html
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
~~~



## 函数

### arguments -参数对象

~~~ javascript
function howManyArgs() {
  alert(arguments.length);
}

howManyArgs("string", 45);
~~~

## DOM 操作

