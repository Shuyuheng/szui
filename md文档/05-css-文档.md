### 滚动条样式

#### 1.隐藏滚动条

~~~css
#el::-webkit-scrollbar { display: none; }
~~~

### sass--预处理器

* 下载包

  ~~~javascript
  npm i node-sass
  ~~~

* 定义变量

  ~~~scss
  $valueColor:red   /*定义变量*/
      .box{
          background-color:$valueColor  /*使用变量*/
          }
  ~~~

### 盒子居中

~~~css
.box{
    position:absolute;
    left:50%;
    top:50%;
    transform:translate(-50%,-50%); /*2d移动*/
}
~~~

### 遮罩盒子穿透

~~~css
.box{
    pointer-events: none; /*不影响下面盒子的点击*/
}
~~~

### 黏性定位

> 粘性定位的盒子会被自己的父元素带走，最多只能移动到父元素底部

~~~css
.box{
    position: -webkit-sticky;
    position: sticky;
    top: 20px;
}
~~~



### 阻止文本选中

~~~css
div{
    -webkit-user-select:none;
	-moz-user-select:none;
	-ms-user-select:none;
    user-select:none;
}
~~~

### css3动画

~~~ javascript
/* 声明 am 动画 */ 
@keyframes am {
    0%{top:38px;color: red}
    50%{top:-38px;color: green}
    100%{top:38px;color: red}
  }
  i:nth-of-type(1){
    position: relative;
    animation: am 5s linear  infinite;  /* 动画名称 动画时间 匀速  循环播放*/
  }
~~~

| linear      | 动画从头到尾的速度是相同的。                   |
| ----------- | ---------------------------------------------- |
| ease        | 默认。动画以低速开始，然后加快，在结束前变慢。 |      |
| ease-in     | 动画以低速开始。                               |
| ease-out    | 动画以低速结束。                               |
| ease-in-out | 动画以低速开始和结束                           |

### 文字超出

~~~css
overflow: hidden;
text-overflow:ellipsis;
white-space: nowrap;  /* 超出部分显示 ... */
~~~

### 鼠标样式

~~~css
*{
      cursor: url('./favicon.ico'),auto;
    }
~~~

### 盒子透明差值

~~~css
mix-blend-mode: difference;
~~~

### 文字颜色渐变

~~~css
 background: linear-gradient(160deg, #778899 20%, transparent 80%);
  -webkit-background-clip: text;
  color: transparent;
~~~





























































































