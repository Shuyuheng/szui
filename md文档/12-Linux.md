### Linux操作系统

> 目前的两大操作系统

Windows：自成派，闭源，收费

Unix-like（像Unix系统）：mac（Darwin） Linux 

> 优缺点

Windows：

​	优点：好用  图形化  各种游戏 软件 丰富

​	缺点：性能差  不安全 病毒多

Linux：

​	优点：性能稳定 快 非常安全

​	缺点：门槛快  基本没有娱乐软件

​	适用于：服务器 工作站...

### 安装Nginx软件按 用于启动服务器

~~~javascript
yum install -y 软件名称
~~~

`yum install -y nginx`

### 查看当前文件夹

> ll 

### 查看当前目录的绝对路径

> pwd

### 切换目录

> cd

### 复制

> cp

### 移动目录

> mv

### 删除目录

> rm -f

### 创建文件夹

> mkdir

### 创建文件

> touch

### 超级管理员 root

> su    // 然后输入密码

### vim编辑器

> vim  ./aa.html
>
> 输入  ：   冒号 进入指令模式   w  表示保存   q  表示退出  esc按键表示退出命令模式
>
> > ：set nu  显示行号
> >
> > ：q 退出   不报错强制退出  q!
> >
> > ：w 保存
>
> 输入 i 键  进入编辑模式    esc表示退出编辑模式
>
> > G: 跳到最后一行
> >
> > 数字G：跳到数字那一行
> >
> > dd：删除光标所在的行
> >
> > 数字dd： 删除数字对应的哪一行
> >
> > p：粘贴
> >
> > yy：复制当前行
> >
> > 数字yy： 复制数字行 n行



### 解压zip

~~~npm
unzip   ***.zip    /*解压到当前文件夹*/
~~~



### 安装Git

~~~linux
/*1.安装*/
yum install curl-devel expat-devel gettext-devel \
  openssl-devel zlib-devel
/*2.验证是否安装成功*/
git --version
/*3.获取本机ssh密钥*/
ssh-keygen -t -b 4096 -C ""
~~~

查看本机Ip                  curl ifconfig.me





~~~nginx
nginx -s stop # 立即停止
nginx -s quit # 停止，在Nginx停止前会等待当前正在进行的任务
nginx -s reload # 重新加载配置文件
~~~



































