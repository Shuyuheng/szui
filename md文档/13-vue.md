### VUE的声明周期

- **beforeCreate**   创建前
- **created**    创建后
- **beforeMount**  挂载前
- **mounted**     挂载后
- **beforeUpdate**   更新前
- **updated**   更新后
- **beforeDestroy**     卸载前
- **destroyed**   卸载后



~~~mysql
INSERT INTO resume (content,user_id) VALUES('8899',7) ON DUPLICATE KEY UPDATE content='8lll';   //有责更新无则插入 主键位user_id
~~~

## 事件修饰符

@click.self="Fn"  只有点自己才触发 类似于阻止冒泡

## .sync修饰符

sync是一个语法糖  <div :aa.sync="aa"></div>

~~~vue
// 就是同时绑定了属性和定义了一个自定义事件
<div :aa.sync="aa" @update:aa="aa"></div>   
// 用于子组件改父组件的值
this.$emit('update:aa',NewValue)
~~~

