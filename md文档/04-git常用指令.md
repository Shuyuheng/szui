### 暂存文件

~~~git
git add .
~~~

### 提交到版本库

~~~git
git commit -m'提交信息备注'
~~~

### 查看状态

~~~ git
git status
~~~

### 查看各个版本

##### 基础信息

~~~git
git reflog
~~~

##### 详细信息

~~~git
git log
~~~

### 版本回退

~~~git
git reset --hard HEAD^   --上一个版本
git reset --hard HEAD^   --上两个个版本 ...以此类推
git reset --hard d7b5    --版本对应的前几位版本号可用于区分即可
~~~

### 拉取代码

> 在已经克隆好厂库的前提下
>
> ~~~git
> git pull
> ~~~

### 推送代码

>[master]--分子名称 如果推送当前分支可以 git push
>
>~~~git
>git push origin master
>~~~

### 分支操作

##### 1. 查看所有分支

~~~git
git branch
~~~

##### 2.切换分支

~~~git
git checkout dev  【dev】分支名称
~~~

##### 3.创建分支

~~~git
git branch dev  【dev】分支名称
~~~

###### 3.1创建并跳转分支

~~~git
git checkout -b dev  【dev】分支名称
~~~

##### 4.删除分支

~~~git
git branch -d
~~~

##### 5.合并分支

~~~git
git merge dev  【dev】被合并的分支名称
~~~

### 修改厂库地址

![image-20200102081537272](mdimgs/image-20200102081537272.png)