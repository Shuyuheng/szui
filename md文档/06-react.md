### 创建项目

~~~javascript
create-react-app name   --创建项目
npm install --save react-router-dom    -- 这样才可以使用路由
~~~



### 组件库

> Ant Design

~~~javascript
npm install antd
~~~

#### form表单基础用法

~~~ javascript
import {Form} from 'antd'


// 在js文件底部注册--并暴露最后结果
export default Form.create()(Login) //挂载到Login组件中
~~~

#### 表单的正则字段

~~~javascript
pattern:/0-9/
~~~

##### 示例：

![1576480550087](mdimgs/1576480550087.png)

#### 消息提示

~~~javascript
message.success('成功提示')
~~~

#### 页面跳转方法

~~~javascript
this.props.history.push('/login')
//如果这个页面是通过函数加载的默认是没有props.shistory这个方法的
//解决办法：
  /*
  	--载入的时候
  	<Route exact path='/' component={props => {this.fn(props)}} />
  	--函数区域
    fn = (props) => {
    	// 展开所有的属性以供使用
    	return <Index {...props}>
    }
  */
~~~





### 路由

~~~javascript
// 引入路由组件
import {BrowserRouter,Route} from 'react-roter-dom'
~~~

~~~html
// 在页面中注册页面
<BrowserRouter>
    <!---exact ---严格匹配模式   path ---路由路径  component  ---对应的组件--> 
    <Route exact path="/" component={Index}></Route>
    <Route exact path="/home" component={Home}></Route>
    <!--component={this.fn} ---可以绑定函数--> 
</BrowserRouter>
~~~

#### 重定向

~~~javascript
import {Redirect} from 'react-router-dom'
~~~

~~~html
<Redirect to="/login"></Redirect>
~~~

#### 示例：

![1576479385863](mdimgs/1576479385863.png)

#### 子路由

* 在想要页面的位置放置代码

  ~~~html
  <Route path="/fu/zhujian" component={Zhujian}>  
  <!-- 不能使用严格匹配：exact 因为要父子页面同时显示 -->
  ~~~

### 生命周期

#### 页面显示出来之后（挂载后）

 * componentDidMout(){// code}







###  eslint-语法忽略

~~~javascript
{/*eslint-disable*/}    // 从当前行开始忽略eslint语法检测
{/*eslint-enable*/}    //  从当前行开始启用eslint语法检测
~~~

### 懒加载

~~~javascript
let Index = React.lazy(() => import('./Index.js'))
// html区域使用 Suspense 标签套起来  fallback 属性是加载中显示的内容
~~~


























