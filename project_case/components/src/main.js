import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 引入sz-ui组件
import Szui from 'sz-ui'
// 引入css文件
import  'sz-ui/dist/sz-ui.css'
// 使用sz-ui
Vue.use(Szui)
// 引入渲染组件

Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
