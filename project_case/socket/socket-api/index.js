var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
// 静态资源托管
// 1.设置静态资源
app.use(require('express').static('public'));
server.listen(8899,() => {
    console.log('http://192.168.10.1:8899')
});

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});
// 当有新连接产生的时候调用该函数
io.on('connection', function (socket) {
  // 给msg这个频道发送信息
  socket.emit('msg', '可以发送信息了哦！');
  // 接收msg这个频道的信息
  socket.on('msg', function (data) {
    // console.log(data);
    // 发送给除了自己的所有人
    socket.broadcast.emit('msg', data);
  });
});