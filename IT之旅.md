# HTML  --超文本语言

## 标签集合

1. pre   用于显示空格 或代码





# CSS  --层叠样式表

~~~css
div{
    letter-spacing:2px;  /* 文字间距 */
    font-family:impact,sans-serif;   /* 价格字体 加粗的 */
} 
#el::-webkit-scrollbar { display: none; } /* 隐藏滚动条 */
/* 盒子居中 */
.box{
    position:absolute;
    left:50%;
    top:50%;
    transform:translate(-50%,-50%); /*2d移动*/
}
table{
    border-collapse:collapse;  /* 边框细线 */
}
/* 自定义滚动条样式 */
*::-webkit-scrollbar,
*::-webkit-scrollbar-thumb {
  width: 26px;
  /*卡槽颜色*/
  background-color: white;
  border-radius: 13px;
   /*北京颜色影响的位置*/
  background-clip: padding-box;
  /*将卡槽挤小*/
  border: 10px solid transparent;
}

*::-webkit-scrollbar-thumb {
      /*内部阴影  滚动条颜色*/         
  box-shadow: inset 0 0 0 10px #7d7da4;
}
~~~



## 字体合集

|       字体        |   作用   |                             截图                             |
| :---------------: | :------: | :----------------------------------------------------------: |
| impact,sans-serif | 数字价格 | ![image-20200325104913941](mdimgs/image-20200325104913941.png) |
|                   |          |                                                              |
|                   |          |                                                              |





# JavaScript --解释性脚本语言

## 操作数组

> map   对每个值重新编辑 并返回新数组

~~~javascript
var arr = [2,3,4,5,6];
var bb= arr.map(function(x){
  return x*x;
});
console.log(bb); // [4, 9, 16, 25, 36]
~~~

## 操作字符串

> includes(*)  ：返回布尔值，判断是否找到参数字符串。

~~~javascript
'aabbcc'.includes('a')   // true
~~~

## 节流/防抖  （高平率触发函数）

> 节流 throttle   当一个函数持续触发时，可以现在每隔x秒执行一次

> 防抖  debounce  当一个函数持续触发，先不执行这个函数 知道不触发时 在执行函数

~~~text
解释网站：http://demo.nimius.net/debounce_throttle/  
npm 包：throttle-debouce
~~~







# Vue  --前端框架

> CSS. extract  阻止css被提取为单独文件



## 组件传值

### provied/inject  父组件给子组件注入属性

 1. 父组件

    ~~~javascript
     provide() {
        return {
          father: this
        };
      }
    ~~~

    

 2. 子组件

    ~~~javascript
    inject:['father'],
    ~~~

### props 类型验证

1. 验证

   ~~~javascript
    // 轮播图数据
       model: {
         // 数据类型
         type: Array,
         // 此属性必填
         required: true
       },
       width: {
         type: [String, Number],
          // 默认值
         default: 700
       },
   ~~~

   

## 事件

|  事件名称  |   事件作用   |
| :--------: | :----------: |
| mousedown  |   鼠标按下   |
|  mouseup   |   鼠标抬起   |
|   click    |   鼠标点击   |
|  dblclick  |   鼠标双击   |
| mousemove  |   鼠标移动   |
| mouseleave |   鼠标离开   |
|  mouseout  |   鼠标移出   |
| mouseenter |  鼠标在内部  |
| mouseover  | 鼠标离开内部 |

## watch属性监听

```javascript
 watch: {
     // 被监听属性名称
    cityName: {
        // 新值    旧值
      handler(newName, oldName) {
      // ...
    },
    deep: true,  //深度监听
    immediate: true  //开局监听
    }
  } 
```

## slot --插槽

## Vue 过度动画

> transition 标签包裹需要动画的元素  通过 v-if  /  v-show  切换入场和退场动画
>
> 官网地址：[https://cn.vuejs.org/v2/guide/transitions.html#JavaScript-%E9%92%A9%E5%AD%90](https://cn.vuejs.org/v2/guide/transitions.html#JavaScript-钩子)

![image-20200323083152505](mdimgs/image-20200323083152505.png)

~~~text
对于这些在过渡中切换的类名来说，如果你使用一个没有名字的 <transition>，则 v- 是这些类名的默认前缀。如果你使用了 <transition name="my-transition">，那么 v-enter 会替换为 my-transition-enter。
~~~









# Node.js  --后端服务器









# Npm --包管理工具

## 自定义vue组件库

~~~javascript
// 根目录创建 packages 目录 该目录为组件的主要文件 所有组件都存放在这里
// 根目录新建一个  vue.config.js  配置文件
~~~

1.   根目录创建 packages 目录 该目录为组件的主要文件 所有组件都存放在这里

   1.1 将所有的组件和字体图标放入 packages 中   （文档：vue的install方法 官网查找）

   1.2 在 packages 中新建一个 index.js 

   ~~~ javascript
   // 整个自定义组件库的一个入口
   
   // 导入所有组件
   import Steep from './Steep'
   // 对应的字体文件也得导入
   // 例如   import '**/***.css'
   
   // 将所有的组件放入数组中 利于遍历注册
   const components = [
       Steep,
   ]
   
   // 必须导出一个 install 方法 或者对象中又 install 方法
   let install = function(Vue){
       // 原理就是 在 vue框架中 调用 Vue.use()  时 会执行这个函数  我们在这个函数中
       // 完成对应的配置即可  注册所有的组件
       components.forEach(item => {
           Vue.component(item.name,item)
       })
       console.log(123)
   
   }
   // 这一步是为了 如果直接引入文件 就不用调用 Vue.use()
   if(typeof window != 'undefined' && window.Vue){
       install(window.Vue)
   }
   // 导出
   export default  {
       install
   }
   ~~~

   

2. 根目录新建一个  vue.config.js  配置文件

   ~~~javascript
   const path = require('path')
   module.exports = {
       // 扩展 webpack 配置 ，使 packages 加入编译
       chainWebpack:config => {
           config.module
           .rule('js')
           .include.add(path.resolve(__dirname,'packages')).end()
           .use('babel')
           .loader('babel-loader')
           .tap(options => {
               // 修改
               return options
           })
       }
   }
   ~~~

   2. 1对 packages 进行打包 想要构建库 需要在 package.json 中加入一行命令 指定打包文件

       ~~~json
   "lib":"vue-cli-service build --target lib packages/index.js"
    // 并将name改为想要的名字  必须是npm上没有的
   "name":"sz-ui"
       ~~~
       
       ![image-20200327160935333](mdimgs/image-20200327160935333.png)
   
~~~npm
   执行打包命令   npm run lib
~~~

3. 将代码放到GitHub上去
   
4. 发布到 Npm 上
   

​	4.1  首先修改package.json 上的  "private": false,

​	4.2  指定包的入口文件在 package.json中添加一行      "main": "dist/zs-ui.umd.js",

​	4.3 在根目录下创建 .npmignore 文件   忽略某些文件不被上传到 npm

~~~text
   # 忽略目录
   src/
   public/
   packages/
   
   # 忽略指定文件
   *.map
   vue.config.js
   babel.config.js
~~~

​	4.4 上传到npm

~~~npm
   nrm sl   // 查看npm源
   npm login  // 登录npm
   npm publish   // 上传  每次上传的版本号必须不一样
~~~

![image-20200319075124424](mdimgs/image-20200319075124424.png)

4.5 如果报错结局流程

 检查是否是淘宝镜像 npm config get registry

 如果是 http://registry.npm.taobao.org/  那么就需要修改

`npm config set registry=http://registry.npmjs.org`

并重新登录

还可能要验证电子邮箱  登录你注册npm是用的邮箱 收件箱里进行验证

![image-20200319081723463](mdimgs/image-20200319081723463.png)

完成~！

提交并切换回淘宝镜像 ``npm config set registry https://registry.npm.taobao.org`

![image-20200327181344034](mdimgs/image-20200327181344034.png)

   # Linux

   > Cmd 登录远程服务器
   >
   > ssh 用户名@远程IP地址    exit退出登录



# Git

### 暂存文件

~~~git
git add .
~~~

### 提交到版本库

~~~git
git commit -m'提交信息备注'
~~~

### 查看状态

~~~ git
git status
~~~

### 查看各个版本

##### 基础信息

~~~git
git reflog
~~~

##### 详细信息

~~~git
git log
~~~

### 版本回退

~~~git
git reset --hard HEAD^   --上一个版本
git reset --hard HEAD^   --上两个个版本 ...以此类推
git reset --hard d7b5    --版本对应的前几位版本号可用于区分即可
~~~

### 拉取代码

> 在已经克隆好厂库的前提下
>
> ~~~git
> git pull
> ~~~

### 推送代码

>[master]--分子名称 如果推送当前分支可以 git push
>
>~~~git
>git push origin master
>~~~

### 分支操作

##### 1. 查看所有分支

~~~git
git branch
~~~

##### 2.切换分支

~~~git
git checkout dev  【dev】分支名称
~~~

##### 3.创建分支

~~~git
git branch dev  【dev】分支名称
~~~

###### 3.1创建并跳转分支

~~~git
git checkout -b dev  【dev】分支名称
~~~

##### 4.删除分支

~~~git
git branch -d
~~~

##### 5.合并分支

~~~git
git merge dev  【dev】被合并的分支名称
~~~

#### 6.添加分支到gitee

~~~git
git push --set-upstream origin name
~~~



### 修改厂库地址

![image-20200102081537272](mdimgs/image-20200102081537272.png)

# nuxt

### 创建nuxt项目

> npx create-nuxt-app  //创建项目
>
> yarm dev 或者  npm  run dev  //启动项目

### 文件目录

| 文件夹名称 |     作用     | 文件类型 |       使用方法       |
| :--------: | :----------: | :------: | :------------------: |
| middleware |    中间件    |    js    |  middleware:'name'   |
| components |   vue组件    |   vue    |     参考vue-cli      |
|  layouts   |   布局组件   |   vue    |     用于页面布局     |
|   pages    |  路由及视图  |   vue    |  自动生成对应的路由  |
|  plugins   |   插件目录   |    js    |      js插件存放      |
|   static   | 静态资源目录 |    *     | 静态资源将会打包至 / |
|            |              |          |                      |
|            |              |          |                      |

![image-20200330210105980](mdimgs/image-20200330210105980.png)

# gitee 静态托管项目



新建一个配置文件 vue.config.js

~~~javascript
module.exports = {
    publicPath:'/aa',  // 改变打包时候的资源地址
}
~~~

美化路径

一个账号上只能有一个不带路径的项目

如果你的厂库名称等于你的账户名称是 就可以省略掉二级目录



![image-20200330114449000](mdimgs/image-20200330114449000.png)

解决nginx history跳转问题

![image-20200330115419709](mdimgs/image-20200330115419709.png)



- [https://blog.csdn.net/u012475575/article/details/90610624 ](https://blog.csdn.net/u012475575/article/details/90610624) 































